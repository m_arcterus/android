    package com.websarva.wings.android.noteview;

    import android.content.Context;
    import android.support.annotation.NonNull;
    import android.support.annotation.Nullable;
    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;
    import android.text.Layout;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.widget.ArrayAdapter;
    import android.widget.ImageView;
    import android.widget.ListView;
    import android.widget.SimpleAdapter;
    import android.widget.TextView;

    import java.util.ArrayList;
    import java.util.HashMap;
    import java.util.List;
    import java.util.Map;
    import java.util.Objects;

    public class MainActivity extends AppCompatActivity {
        private static final String[] FROM = {"noteImage", "noteTitle", "author", "viewCount", "likeCount", "commentCount"};
        private static final int[] TO = {R.id.note_image, R.id.note_title, R.id.author_name, R.id.view_counts, R.id.like_counts, R.id.comment_counts};

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            ListView lvNotes = findViewById(R.id.lvNotes);
            List<NoteBook> noteList = new ArrayList<>();

            NoteBook note = new NoteBook(R.drawable.note_image, "期末対策", "アルクテラス", 300, 50, 10);
            noteList.add(note);

            note = new NoteBook(R.drawable.note_image2, "復習テキスト", "アルクテラス", 20, 30, 5);
            noteList.add(note);

            note = new NoteBook(R.drawable.note_image3, "国語テキスト", "アルクテラス", 25, 5, 0);
            noteList.add(note);


            ArrayAdapter adapter = new NoteBookAdapter(MainActivity.this, R.layout.notelistview, noteList);
            lvNotes.setAdapter(adapter);
        }

        public static class NoteBookAdapter extends ArrayAdapter<NoteBook> {
            List<NoteBook> notelist;
            public NoteBookAdapter(@NonNull Context context, int resource, List<NoteBook> notelist) {
                super(context, resource);
                this.notelist = notelist;
            }

            @Override
            public int getCount() {
                return notelist.size();
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                ViewHolder viewholder;

                if(convertView == null) {
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.notelistview, parent, false);
                    viewholder = new ViewHolder();
//                    noteBook.image = noteBookView.findViewById((R.id.note_image));
//                    noteBook.noteTitle = noteBookView.findViewById(R.id.note_title);
//                    noteBook.author = noteBookView.findViewById(R.id.author_name);
//                    noteBook.likeCount = noteBookView.findViewById(R.id.like_counts);
//                    noteBook.viewCount = noteBookView.findViewById(R.id.view_counts);
//                    noteBook.commentCount = noteBookView.findViewById(R.id.comment_counts);
                    viewholder.noteImage = convertView.findViewById(R.id.note_image);
                    viewholder.noteTitle = convertView.findViewById(R.id.note_title);
                    viewholder.author = convertView.findViewById(R.id.author_name);
                    viewholder.likeCount = convertView.findViewById(R.id.like_counts);
                    viewholder.viewCount = convertView.findViewById(R.id.view_counts);
                    viewholder.commentCount = convertView.findViewById(R.id.comment_counts);
                    convertView.setTag(viewholder);
                }else {
                    viewholder = (ViewHolder) convertView.getTag();
                }

//                noteBook.image.setImageResource(notelist.get(position).image);
//                noteBook.noteTitle.setText(notelist.get(position).noteTitle);
//                noteBook.author.setText(notelist.get(position).author);
//                noteBook.viewCount.setText(String.valueOf(notelist.get(position).likeCount));
//                noteBook.likeCount.setText(String.valueOf(notelist.get(position).viewCount));
//                noteBook.commentCount.setText(String.valueOf(notelist.get(position).commentCount));
                
                viewholder.noteImage.setImageResource(notelist.get(position).image);
                viewholder.noteTitle.setText(notelist.get(position).noteTitle);
                viewholder.author.setText(notelist.get(position).author);
                viewholder.likeCount.setText(String.valueOf(notelist.get(position).likeCount));
                viewholder.viewCount.setText(String.valueOf(notelist.get(position).viewCount));
                viewholder.commentCount.setText(String.valueOf(notelist.get(position).commentCount));

                return convertView;
            }

        }

        public static class NoteBook{
            int image, likeCount, commentCount, viewCount;
            String noteTitle, author;
            NoteBook(int image, String noteTitle, String author, int viewCount, int likeCount, int commentCount) {
                this.image = image;
                this.noteTitle = noteTitle;
                this.author = author;
                this.viewCount = viewCount;
                this.likeCount = likeCount;
                this.commentCount = commentCount;

            }
        }

        private static class ViewHolder{
            ImageView noteImage;
            TextView noteTitle, author, likeCount, viewCount, commentCount;
        }
    }
